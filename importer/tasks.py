from celery import task, current_task
from nicecrap import digikala, meghdad, bamilo, modiseh

@task()
def import_products(shop_name, category):
    for i in range(100):
        if shop_name.lower() is 'digikala':
            scraper = digikala
        elif shop_name.lower() is 'bamilo':
            scraper = bamilo
        elif shop_name.lower is 'meghdad':
            scraper = meghdad
        elif shop_name.lower() is 'modiseh':
            scraper = modiseh
        else:
            return False

        num_of_pages = int(development_configs[shop_name][cat]['pages_to_scrap'])
        progress_percentage = 0
        progress_unit = abs(100/num_of_pages*products_per_page)
        for page_num in range(num_of_pages):
            print('im on page'+str(page_num)+'\n')
            page = scraper._get_page(page_num+1, development_configs[shop_name]['http_library'], development_configs['digikala'][cat]['products_url'])
            print('starting to get pros\n')
            page_items = scraper._get_products_on_page(page, development_configs[shop_name]['http_library'], cat)
            products = scraper.convert_pretty_list_to_models(page_items)
            for p in products:
                prod = ProductPage()
                try:
                    try:
                        temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                        print('not here')
                    except ProductPage.DoesNotExist:
                        temp = None
                        print('here')

                    if temp is not None:
                        print('temp is Not None')
                        progress_percentage += progress_unit
                        continue

                    else:
                        prod.fill_model_with_scraped_obj(p)
                        prod.save()
                        progress_percentage += progress_unit
                        print("saved {}".format(prod.id))

                    current_task.update_state(state='PROGRESS',
                                    meta={'current': progress_percentage, 'total': 100})

                except Exception as e:
                    print("something happend\n" + str(e))
                    continue
