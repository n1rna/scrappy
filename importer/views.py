from django.shortcuts import render
from celery.result import AsyncResult

from django.http import HttpResponse, HttpResponseRedirect
import json
from .tasks import import_products
# Create your views here.

def poll_state(request):
    job_id = request.GET.get('job', '')
    if job_id == '':
        return HttpResponse('No job id given.')

    job = AsyncResult(job_id)
    data = job.result or job.state
    return HttpResponse(json.dumps(data), mimetype='application/json')


def init_product_import(request):
    """ A view to start a background job and redirect to the status page """

    shop_name = request.GET.get('shop', '')
    category = request.GET.get('category', '')
    if shop_name is '' or category is '':
        return HttpResponse('check your parameters')

    job = import_products.delay(shop_name, category)
    return HttpResponseRedirect(reverse('poll_state') + '?job=' + job.id)
