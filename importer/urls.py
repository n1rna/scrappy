from django.urls import path, include
from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('products', views.init_product_import, name='import_products'),
    path('poll_state', views.poll_state, name='poll_state'),
]
