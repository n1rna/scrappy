import React, { Component } from 'react';
import ProductItem from './ProductItem';
import postApi from '../postApi'
import queryString from 'query-string';


class ProductsList extends Component {

  constructor (props) {
    super(props)
    this.state = {
      data: [],
    }
    this.changeData = this.changeData.bind(this)

  }

  componentDidMount () {
    let params = queryString.parse(this.props.location.search)
    postApi.getProductsList(parseInt(params.page, 10), params.shop, params.category).then(res => {
      this.setState({
        data: res,
      })
    })

  }

  changeData (newData) {
   this.setState({
     data: newData
   })
  }

  render () {

      const products = this.state.data.map(product => {
        return <ProductItem
          key={product.id}
          product={product} />
      })
      return (
          <div className="container-fluid">
            <div className="row">
            {products}
            </div>
          </div>
      )
  }

}

export default ProductsList;
