import React, { Component } from 'react'

class ProductItem extends Component {
  constructor () {
    super()

  }

  render () {
    const { id, english_name, url } = this.props.product
    return (
      <div className="col-md-6">
          <div className="card">
              <div className="card-horizon">
                  <div className="image-container col-md-4">
                      <div className="background-holder has-content"></div>
                  </div>
                  <div className="container-fluid">
                      <div className="row">
                          <div className="col-md-8 ml-auto">
                              <div className="pdd-vertical-30 pdd-horizon-25">
                                  <h2 className="mrg-btm-20">{id}</h2>
                                  <p>{english_name}</p>
                                  <p></p>
                                  <div className="mrg-top-25">
                                      <a href="products/{{p.id}}" className="btn btn-primary no-mrg-btm">Details</a>
                                  </div>

                              </div>
                          </div>
                  </div>
              </div>
          </div>
      </div>

    </div>

    )
  }
}

export default ProductItem
