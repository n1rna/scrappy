import React, { Component } from 'react';
import ProductsList from './ProductsList';
import { Switch, Route, Link } from 'react-router-dom'


class Main extends Component {
  render () {
    return (
      <div>
        <Switch>
          <Route exact path='/rv/' component={ProductsList} />
          <Route exact path='/rv/products' render={(props) => (
            <ProductsList {...props} data={this.props.data} />
          )} />
        </Switch>
      </div>
    )
  }
}
export default Main;
