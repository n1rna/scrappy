import React, { Component } from 'react';
import http from 'axios'
import _ from 'lodash'
import postApi from './postApi'
import Main from './components/Main'
import {Link, Switch, Route, BrowserRouter} from 'react-router-dom'
import ProductsList from './components/ProductsList'
import queryString from 'query-string';

class App extends Component {

  constructor (props) {
    super(props)
    this.state = {
      data: []
    }
    this.changeData = this.changeData.bind(this)

  }

  componentDidMount () {
    this.setState({
      data: []
    })

  }

  changeData (newData) {
   this.setState({
     data: newData
   })
  }

  render() {
    return (

      <div>
        <Main data={this.state.data} />
     </div>
    );
  }
}

export default App;
