
import axios from "axios";

let scrappy_products_api = 'http://localhost:8000/scrappy/api/v0.0/products/?format=json'
let similars_endpoint = '/similars/'

export default {
  requestPost: function(id) {
    return axios.get(`http://127.0.0.1:8000/scrappy/api/`)
      .then(response => {
         return {
           title: response.data.title,
           image: "http://lorempixel.com/c/880/200/technics/",
           content: '<p>' + response.data.body + '</p>'
         };
      })
  },

  getProductsList: function(page_num, shop_name='', category='') {
    let url = scrappy_products_api+'&page='+page_num+'&shop='+shop_name+'&category'+category
    return axios.get(url)
      .then(response => {
        return response.data;
      })
  },
}
