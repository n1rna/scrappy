# Scrappy

## Overview
Django application to scrape online shopping websites.
Digikala, Meghdad, Modiseh, Bamilo support added.

## Usage

```
pip install -r requirements &&
python manage.py runserver 0.0.0.0:8080
```
