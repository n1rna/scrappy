'''
    ScrapedProduct Django Rest Framework Model Serializer
    ##

    '''
from rest_framework import serializers
from nicecrap.models import ScrapedProduct
from functionality.models import ProductPage, SHOP_CHOICES, CATEGORY_CHOICES

class ProductPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductPage
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.persian_name = validated_data.get('persian_name', instance.persian_name)
        instance.english_name = validated_data.get('english_name', instance.english_name)
        instance.brand = validated_data.get('brand', instance.brand)
        instance.category = validated_data.get('category', instance.category)
        instance.price = validated_data.get('price', instance.price)

        instance.original_image_url = validated_data.get('image_url', instance.image_url)
        instance.original_url = validated_data.get('original_url', instance.original_url)
        instance.origin_shop_name = validated_data.get('origin_shop_name', instance.origin_shop_name)

        instance.tech_specs = validated_data.get('tech_spec_raw', instance.tech_spec_raw)

        instance.save()
        return instance
