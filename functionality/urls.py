from django.urls import path, include
from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', views.ProductPageList.as_view(), name='products_list'),
    path('products/<int:product_id>/', views.product_matching, name='product_details'),
    path('products/match/', views.match_two_products, name='match_two_products'),
    path('api/v0.0/products/', views.ProductViewSet.as_view({'get': 'list'}), name='api_test'),
    path('api/v0.0/products/<int:pk>/', views.ProductViewSet.as_view({'get': 'retrieve'}), name='api_tost'),
    path('api/v0.0/products/<int:pk>/similars/', views.ProductViewSet.as_view({'get': 'similars'}), name='api_shost'),
]
