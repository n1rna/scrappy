from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models
import json
import urllib3


connections.create_connection()

elasticsearch_api_endpoint = 'http://127.0.0.1:9200/productpage-index/_search'

class ProductPageIndex(DocType):
    persian_name = Text()
    english_name = Text()

    brand = Text()
    category = Text()
    price = Text()

    tech_specs = Text()

    class Meta:
        index = 'productpage-index'

def bulk_indexing():
    ProductPageIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing() for b in models.ProductPage.objects.all().iterator()))

def search_all_docs(search_entry, page_num):
    items_per_page = 20

    request_body = json.dumps(
        {
          "from": (page_num-1)*items_per_page, "size": items_per_page,
          "query": {
            "match": {
              "english_name": search_entry,
            }
          }
        }
    )

    http = urllib3.PoolManager()

    es_response = http.request('POST', elasticsearch_api_endpoint,
                     headers={'Content-Type': 'application/json'},
                     body=request_body)

    esr_json = json.loads(es_response.data)
    search_hits = []
    for hit in esr_json['hits']['hits']:
        product_obj = models.ProductPage.objects.get(id=int(hit['_id']))
        search_hits.append(product_obj.id)

    return search_hits

def search_for_similars(product_id):
    try:
        product = models.ProductPage.objects.get(id=product_id)
    except models.ProductPage.DoesNotExist:
        return None

    items_in_req = 15

    request_body = json.dumps(
        {
            "from" : 0, "size" : items_in_req,
            "query": {
                "function_score": {
                    "query":{ "match": { "english_name": product.english_name} },
                    "boost": "5",
                    "functions": [
                      {
                          "filter": { "match": { "english_name": product.english_name } },
                          "weight": 40
                      },
                      {
                          "filter": { "match": { "tech_specs": product.tech_specs } },
                          "weight": 13
                      },
                      {
                          "filter": { "match": { "brand": product.brand } },
                          "weight": 10,
                      }
                  ],
                    "boost_mode":"multiply"
                }
            }
        }
    )

    http = urllib3.PoolManager()

    es_response = http.request('POST', elasticsearch_api_endpoint,
                     headers={'Content-Type': 'application/json'},
                     body=request_body)

    esr_json = json.loads(es_response.data)
    similar_products_list = []
    for hit in esr_json['hits']['hits']:
        product_obj = models.ProductPage.objects.get(id=int(hit['_id']))
        similar_products_list.append(product_obj)

    return similar_products_list
