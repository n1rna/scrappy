api_config = {
    'digikala': {
        'pages_to_scrap': 10,
        'library_for_products_list': 'dryscrape',
        'library_for_product_details': 'dryscrape',
    },

    'bamilo': {
        'pages_to_scrap': 10,
        'library_for_products_list': 'requests',
        'library_for_product_details': 'requests',
    },

    'modiseh': {
        'pages_to_scrap': 10,
        'library_for_products_list': 'requests',
        'library_for_product_details': 'requests',
    },

    'meghdad': {
        'pages_to_scrap': 10,
        'library_for_products_list': 'urllib3',
        'library_for_product_details': 'urllib3',
    }
}
