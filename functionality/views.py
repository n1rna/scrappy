from .models import ProductPage, BaseProduct
from .serializers import ProductPageSerializer
from .config import api_config

from django.shortcuts import render, redirect
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins

from django.shortcuts import get_object_or_404
from rest_framework import viewsets

from nicecrap import digikala, bamilo, modiseh, meghdad
from django.views.generic import ListView
from .search import search_all_docs

def frontpage(request):
    context = {
        'title': 'this is title',
        'sth': 'sth else!',
    }
    return render(request, 'index.html', context)

def api_test(request):
    from django.http import JsonResponse
    return JsonResponse({'title':'bar','body':'kir','id':4, 'userId': 3})


def products_list(request):
    all_products = ProductPage.objects.all()

    context = {
        'products': all_products,

    }

    return render(request, 'products_list.html', context)


class ProductViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin):
    """
    A simple ViewSet for listing or retrieving products.
    """
    queryset = ProductPage.objects.all()

    def list(self, request):
        category = self.request.GET.get('category', '')
        shop_name = self.request.GET.get('shop', '')

        if category != '':
            try:
                qset_c = ProductPage.objects.filter(category=category)
            except:
                qset_c = ProductPage.objects.all()
        else:
            qset_c = ProductPage.objects.all()

        if shop_name != '':
            try:
                qset_s = ProductPage.objects.filter(origin_shop_name=shop_name)
            except:
                qset_s = ProductPage.objects.all()
        else:
            qset_s = ProductPage.objects.all()

        queryset = qset_s & qset_c
        page = request.GET.get('page')

        try:
            page = self.paginate_queryset(queryset)
        except Exception as e:
            page = []
            data = page
            return Response({
                "status": status.HTTP_404_NOT_FOUND,
                "message": 'No more record.',
                "data" : data
                })

        if page is not None:
            serializer = ProductPageSerializer(page, many=True)
            return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = ProductPage.objects.all()
        product = get_object_or_404(queryset, pk=pk)
        serializer = ProductPageSerializer(product)
        return Response(serializer.data)

    def similars(self, request, pk=None):
        product = get_object_or_404(ProductPage.objects.all(), pk=pk)
        queryset = product.similars()
        serializer = ProductPageSerializer(queryset, many=True)
        return Response(serializer.data)


class ProductPageList(ListView):
    model = ProductPage
    template_name = 'products_list.html'
    context_object_name = 'products_list'
    paginate_by = 20

    def get_queryset(self):
        category = self.request.GET.get('category', '')
        shop_name = self.request.GET.get('shop', '')
        search_query = self.request.GET.get('s', '')

        qset_objects_not_verified = ProductPage.objects.filter(has_match=False)

        if category != '':
            try:
                qset_c = ProductPage.objects.filter(category=category)
            except:
                qset_c = ProductPage.objects.all()
        else:
            qset_c = ProductPage.objects.all()

        if shop_name != '':
            try:
                qset_s = ProductPage.objects.filter(origin_shop_name=shop_name)
            except:
                qset_s = ProductPage.objects.all()
        else:
            qset_s = ProductPage.objects.all()

        if search_query != '':
            list_search_query = search_all_docs(search_query, int(self.request.GET.get('page', '1')))
            qset_search_query = ProductPage.objects.filter(pk__in=list_search_query)
        else:
            qset_search_query = ProductPage.objects.all()
        return qset_s & qset_c & qset_search_query & qset_objects_not_verified

def product_matching(request, product_id):

    try:
        product = ProductPage.objects.get(id=product_id)
    except ProductPage.DoesNotExist:
        context = {
            'message': 'Product Not Found',
        }
        return render(request, 'product.html', context)

    similar_products = product.similars()

    context = {
        'main_product' : product,
        'similar_products' : similar_products,
    }
    return render(request, 'product.html', context)

def match_two_products(request):
    primary_product_id = request.POST.get('primary_product_id', '')
    secondary_product_id = request.POST.get('secondary_product_id', '')

    if primary_product_id == '' or secondary_product_id == '':
        context = {
            'message' : 'something went wrong' + ':' + 'fields are empty',
        }
        return render(request, 'error.html', context)

    try:
        primary_product = ProductPage.objects.get(id=int(primary_product_id))
    except ProductPage.DoesNotExist:
        primary_product = None

    try:
        secondary_product = ProductPage.objects.get(id=int(secondary_product_id))
    except ProductPage.DoesNotExist:
        secondary_product = None

    if not primary_product or not secondary_product:
        context = {
            'message' : 'something went wrong' + ':' + 'products not found',
        }
        return render(request, 'error.html', context)

    base_product = BaseProduct()
    base_product.save()
    try:
        primary_product.base_product = base_product
        primary_product.has_match = True
        primary_product.save()

    except Exception as e:
        context = {
            'message' : 'something went wrong' + ':' + str(e),
        }
        return render(request, 'error.html', context)

    try:
        secondary_product.base_product = base_product
        secondary_product.has_match = True
        secondary_product.save()

    except Exception as e:
        context = {
            'message' : 'something went wrong' + ':' + str(e),
        }
        return render(request, 'error.html', context)

    return redirect('product_details', product_id=primary_product.id)
