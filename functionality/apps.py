from django.apps import AppConfig


class FunctionalityConfig(AppConfig):
    name = 'functionality'
    
    def ready(self):
        import functionality.signals
