from django.db import models
from rake_nltk import Rake
from googletrans import Translator
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from .search import ProductPageIndex


# Create your models here.

CELLPHONE = 'Cellphone'
HOMEAPPLIANCE = 'Home-Appliance'
WEARABLE = 'Wearable'

CATEGORY_CHOICES = (
    (CELLPHONE, 'گوشی موبایل'),
    (HOMEAPPLIANCE, 'لوازم خانگی'),
    (WEARABLE, 'پوشاک'),
)

DIGIKALA = 'Digikala'
MEGHDADIT = 'Meghdad'
BAMILO = 'Bamilo'
MODISEH = 'Modiseh'

SHOP_CHOICES = (
    (DIGIKALA, 'دیجیکالا'),
    (MEGHDADIT, 'مقداد آی تی'),
    (BAMILO, 'بامیلو'),
    (MODISEH, 'مدیسه'),
)

class BaseProduct(models.Model):
    category = models.CharField(max_length=80, choices=CATEGORY_CHOICES, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class ProductPage(models.Model):
    persian_name = models.CharField(max_length=200, null=True, blank=False)
    english_name = models.CharField(max_length=200, null=True, blank=False)

    brand = models.CharField(max_length=80, null=True, blank=False)
    category = models.CharField(max_length=80, choices=CATEGORY_CHOICES)
    price = models.CharField(max_length=100, null=True, blank=False)

    origin_shop_name = models.CharField(max_length=80, choices=SHOP_CHOICES)
    original_url = models.URLField()
    identification_in_origin = models.CharField(max_length=80, null=True, blank=False)

    original_image_url = models.URLField()
    image = models.ImageField()

    tech_specs = models.TextField()

    timestamp = models.DateTimeField(auto_now_add=True)

    base_product = models.ForeignKey(BaseProduct, related_name='matches', null=True, on_delete=models.PROTECT)
    has_match = models.NullBooleanField(default=False, null=True)



    class Meta:
        ordering = ('timestamp',)

    def fill_model_with_scraped_obj(self, scraped):
        self.persian_name = scraped.persian_name
        self.english_name = scraped.english_name
        self.original_url = scraped.original_url
        self.original_image_url = scraped.image_url
        self.price = scraped.price
        self.brand = scraped.brand
        self.origin_shop_name = scraped.origin_shop_name
        self.tech_specs = scraped.tech_spec_raw
        self.identification_in_origin = scraped.identification_in_origin

        self.category = CATEGORY_CHOICES[0]

        for c in CATEGORY_CHOICES:
            if scraped.category == c[0]:
                self.category = c


    def save(self, *args, **kwargs):

        '''
            if self.original_image_url and not self.image:
            from io import BytesIO
            from urllib.request import urlopen
            from django.core.files import File

            response = urlopen(self.original_image_url)
            io = BytesIO(response.read())
            self.image.save(self.original_image_url.split('/')[-1], File(io))
        '''
        return super(ProductPage, self).save(*args, **kwargs)


    '''
        Use google translate to translate technical \
        specifications to english
    '''
    @property
    def translated_tech_specs(self):
        translator = Translator()
        return translator.translate(self.tech_spec_raw, src='fa').text

    '''
        Use NLTK algorithm to extract keywords out of\
        technical specifications.
    '''
    @property
    def keywords_list(self):
        r = Rake()
        r.extract_keywords_from_text(self.translated_tech_specs)
        return r.get_ranked_phrases()

    def similars(self):
        '''
        same_category_products = ProductPage.objects.all()#filter(category=self.category)
        choices = [x.english_name+str(':')+str(x.id) for x in same_category_products]
        relateds = process.extract(self.english_name, [c for c in choices], limit=15)

        relateds_list = [ProductPage.objects.get(id=int(r[0].split(':')[-1])) for r in relateds]
        return relateds_list
        '''
        '''
        using elasticsearch from now on
        '''
        from .search import search_for_similars
        return search_for_similars(self.id)

    def indexing(self):
       obj = ProductPageIndex(
          meta={'id': self.id},
          persian_name=self.persian_name,
          english_name=self.english_name,
          brand=self.brand,
          category=self.category,
          price=self.price,
          tech_specs=self.translated_tech_specs
       )
       obj.save()
       return obj.to_dict(include_meta=True)
