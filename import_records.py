from nicecrap import digikala, bamilo, modiseh, meghdad
from nicecrap.config import development_configs, nicecrap_config
from functionality.models import ProductPage

def init_nicecrap(scraper, cat, shop_name):
    num_of_pages = int(development_configs[shop_name][cat]['pages_to_scrap'])
    for page_num in range(num_of_pages):
        print('im on page'+str(page_num)+'\n')
        page = scraper._get_page(page_num+1, development_configs[shop_name]['http_library'], development_configs['digikala'][cat]['products_url'])
        print('starting to get pros\n')
        page_items = scraper._get_products_on_page(page, development_configs[shop_name]['http_library'], cat)
        products = scraper.convert_pretty_list_to_models(page_items)
        for p in products:
            prod = ProductPage()
            try:
                try:
                    temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                    print('not here')
                except ProductPage.DoesNotExist:
                    temp = None
                    print('here')

                if temp is not None:
                    print('temp is Not None')
                    continue

                else:
                    prod.fill_model_with_scraped_obj(p)
                    prod.save()
                    print("saved {}".format(prod.id))

            except Exception as e:
                print("something happend\n" + str(e))
                continue

def digikala_init(scraper, cat):
    num_of_pages = int(development_configs['digikala'][cat]['pages_to_scrap'])
    for page_num in range(num_of_pages):
        print('im on page'+str(page_num)+'\n')
        page = scraper._get_page(page_num+1, 'dryscrape', development_configs['digikala'][cat]['products_url'])
        print('starting to get pros\n')
        page_items = scraper._get_products_on_page(page, 'dryscrape', cat)
        products = scraper.convert_pretty_list_to_models(page_items)
        for p in products:
            prod = ProductPage()
            try:
                try:
                    temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                    print('not here')
                except ProductPage.DoesNotExist:
                    temp = None
                    print('here')

                if temp is not None:
                    print('temp is Not None')
                    continue

                else:
                    prod.fill_model_with_scraped_obj(p)
                    prod.save()
                    print("saved {}".format(prod.id))

            except Exception as e:
                print("something happend\n" + str(e))
                continue


def bamilo_init(scraper, cat):
    num_of_pages = int(development_configs['bamilo'][cat]['pages_to_scrap'])
    for page_num in range(num_of_pages):
        page = scraper._get_page(page_num+1, 'requests', development_configs['bamilo'][cat]['products_url'])
        page_items = scraper._get_products_on_page(page, 'requests', cat)
        products = scraper.convert_pretty_list_to_models(page_items)
        for p in products:
            prod = ProductPage()
            try:
                try:
                    temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                    print('not here')
                except ProductPage.DoesNotExist:
                    temp = None
                    print('here')

                if temp is not None:
                    print('temp is Not None' + temp.identification_in_origin + ':' + p.identification_in_origin)
                    continue

                else:
                    prod.fill_model_with_scraped_obj(p)
                    prod.save()
                    print("saved {}".format(prod.id))

            except Exception as e:
                print("something happend\n" + str(e))
                continue

def meghdad_init(scraper, cat):
    num_of_pages = int(development_configs['meghdadit'][cat]['pages_to_scrap'])
    for page_num in range(num_of_pages):
        page = scraper._get_page(page_num+1, 'requests', development_configs['meghdadit'][cat]['products_url'])
        page_items = scraper._get_products_on_page(page, 'requests', cat)
        products = scraper.convert_pretty_list_to_models(page_items)
        for p in products:
            prod = ProductPage()
            try:
                try:
                    temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                    print('not here')
                except ProductPage.DoesNotExist:
                    temp = None
                    print('here')

                if temp is not None:
                    print('temp is Not None')
                    continue

                else:
                    prod.fill_model_with_scraped_obj(p)
                    prod.save()
                    print("saved {}".format(prod.id))

            except Exception as e:
                print("something happenf\n" + str(e))
                continue


def modiseh_init(scraper, cat):
    num_of_pages = int(development_configs['modiseh'][cat]['pages_to_scrap'])
    for page_num in range(num_of_pages):
        page = scraper._get_page(page_num+1, 'requests', development_configs['modiseh'][cat]['products_url'])
        page_items = scraper._get_products_on_page(page, 'requests', cat)
        products = scraper.convert_pretty_list_to_models(page_items)
        for p in products:
            prod = ProductPage()
            try:
                try:
                    temp = ProductPage.objects.get(identification_in_origin=p.identification_in_origin)
                    print('not here')
                except ProductPage.DoesNotExist:
                    temp = None
                    print('here')

                if temp is not None:
                    print('temp is Not None' + temp.identification_in_origin + ':' + p.id_in_origin)
                    continue

                else:
                    prod.fill_model_with_scraped_obj(p)
                    prod.save()
                    print("saved {}".format(prod.id))

            except Exception as e:
                print("something happend\n" + str(e))
                continue
