from bs4 import BeautifulSoup
import string
import re
from .config import development_configs
from .models import ScrapedProduct
from functionality.models import ProductPage

class ShopScraper():

    def __init__(self, url, name, pagination_postfix):
        self.url = url
        self.pagination_postfix = pagination_postfix
        self.name = name

    '''
        Use this in case you're facing javascript rendered content
        url -- url to fetch
        ##
        return dryscrape session response
    '''
    @staticmethod
    def fetch_using_dryscrape(url):
        import dryscrape

        session = dryscrape.Session()
        session.visit(url)
        response = session.body()
        return response

    '''
        Both urllib3 and requests do the same thing
        No differences here
        url -- url to fetch
        ##
        return raw request response
    '''
    @staticmethod
    def fetch_using_requests(url):
        import requests

        response = requests.get(url)
        return response.content

    @staticmethod
    def fetch_using_urllib3(url):
        import urllib3

        http = urllib3.PoolManager()
        response = http.request('GET', url)
        return response.data

    '''
        Convert raw request response to soup object
        ... Using bs4
        raw -- raw request response
        ##
        return soup object
    '''

    @staticmethod
    def convert_raw_to_soup(raw):
        soup = BeautifulSoup(raw, 'html.parser')
        return soup

    @staticmethod
    def _get_url_soup(lib, url):
        if not url:
            return 'Url cannot be None'

        if lib is 'dryscrape':
            raw = ShopScraper.fetch_using_dryscrape(url)
            return ShopScraper.convert_raw_to_soup(raw)

        elif lib is 'requests':
            raw = ShopScraper.fetch_using_requests(url)
            return ShopScraper.convert_raw_to_soup(raw)

        elif lib is 'urllib3':
            raw = ShopScraper.fetch_using_urllib3(url)
            return ShopScraper.convert_raw_to_soup(raw)

        else:
            return None

    def _get_page(self, page_number, lib, base_url):
        if page_number > 1:
            url_to_fetch = base_url + self.pagination_postfix + str(page_number) + ('' if self.name != 'Meghdad' else '/')
        else:
            url_to_fetch = base_url

        return ShopScraper._get_url_soup(lib, url_to_fetch)

    '''
        Important features are:
            link, image, persian_name, english_name, tech_spec
    '''
    @staticmethod
    def _get_products_on_page__digikala(page_soup, lib, cat):

        items = page_soup.find_all('div', class_='products__item')
        products_list = []
        for p in items:

            link = re.match('/Product/DKP-[0-9]{1,6}/',
                str(p.select('.products__item-fatitle')[0].get('href'))).group(0)

            link = development_configs['digikala']['absolute_url'] + link

            try:
                temp = ProductPage.objects.get(original_url=link)
            except ProductPage.DoesNotExist:
                temp = None

            if temp is not None:
                continue

            details_soup =  ShopScraper._get_url_soup(lib, link)

            if not details_soup:
                return 'Error in getting url soup'

            image = details_soup.select('.thumb-image')[0].img.get('src')
            persian_name = details_soup.select('.info-header')[0].h1.contents[0].string
            english_name = details_soup.select('.info-header')[0].h1.contents[1].string

            price = details_soup.select('#frmLblPayablePriceAmount')[0].text
            brand = english_name.split(' ')[0]
            origin_shop_name = 'Digikala'
            category = cat
            id_pattern = re.compile('DKP-[0-9]{1,6}')
            identification_in_origin = re.search(id_pattern, link).group(0)

            tech_spec = details_soup.select('#frmSecTechSpecs')[0].text

            entity = dict(link=link, image=image, tech_spec=tech_spec,
                            persian_name=persian_name, english_name=english_name,
                            price=price, brand=brand, category=cat, id_in_origin=identification_in_origin,
                            origin_shop_name=origin_shop_name)

            products_list.append(entity)

        return products_list


    @staticmethod
    def _get_products_on_page__bamilo(page_soup, lib, cat):
        items = page_soup.find_all('div', class_='sku')
        products_list = []

        for p in items:

            link = p.select('.link')[0].get('href')

            try:
                temp = ProductPage.objects.get(original_url=link)
            except ProductPage.DoesNotExist:
                temp = None

            if temp is not None:
                print('im here')
                continue

            details_soup = ShopScraper._get_url_soup(lib, link)

            if not details_soup:
                return 'Error in getting url soup'

            try:
                image = details_soup.select('#productImage')[0].get('data-src')
            except:
                image = ''

            persian_name = details_soup.find('span', attrs={'itemprop':'name'}).text

            from googletrans import Translator
            english_name = Translator().translate(persian_name, src='fa').text

            price = details_soup.select('.price-box')[0].text
            brand = persian_name.split(' ')[0]
            origin_shop_name = 'Bamilo'
            category = cat
            id_pattern = re.compile('[0-9a-zA-Z]{1,8}\.html')
            identification_in_origin = re.search(id_pattern, link).group(0).split('.')[0]

            tech_spec = details_soup.select('.product-description')[0].text + '\n\n'
            tech_spec += details_soup.select('#product-details')[0].text

            entity = dict(link=link, image=image, tech_spec=tech_spec,
                            persian_name=persian_name, english_name=english_name,
                            price=price, brand=brand, category=category, id_in_origin=identification_in_origin,
                            origin_shop_name=origin_shop_name)

            products_list.append(entity)

        return products_list


    @staticmethod
    def _get_products_on_page__meghdad(page_soup, lib, cat):
        items = page_soup.find_all('div', class_='item')
        products_list = []

        i = 0
        for p in items:

            link = p.select('#ContentPlaceHolder1_rptList_hlkTitle_'+str(i))[0].get('href')
            i+=1
            abs_url = development_configs['meghdadit']['absolute_url']
            link = abs_url + link

            details_soup = ShopScraper._get_url_soup(lib, link)

            if not details_soup:
                return 'Error in getting url soup'

            try:
                image = details_soup.select('#ContentPlaceHolder1_imgItem')[0].get('src')
            except:
                image = ''
            persian_name = details_soup.select('#ContentPlaceHolder1_lblItemTitle2')[0].text
            english_name = details_soup.select('#ContentPlaceHolder1_lblItemTitle')[0].text

            try:
                price = details_soup.select('#ContentPlaceHolder1_rptGheymat_lblGheymat_0')[0].text
            except:
                price = ''

            brand = english_name.split(' ')[0]
            origin_shop_name = 'Meghdad'
            category = cat
            id_pattern = re.compile('/product/[0-9]{1,6}/')
            identification_in_origin = re.search(id_pattern, link).group(0).split('/')[2]

            tech_spec = ''
            tech_spec_selectors = details_soup.select('.detail-table')
            for tss in tech_spec_selectors:
                tech_spec += tss.text + '\n\n'

            entity = dict(link=link, image=image, tech_spec=tech_spec,
                            persian_name=persian_name, english_name=english_name,
                            price=price, brand=brand, category=category, id_in_origin=identification_in_origin,
                            origin_shop_name=origin_shop_name)

            products_list.append(entity)

        return products_list

    @staticmethod
    def _get_products_on_page__modiseh(page_soup, lib, cat):
        items = page_soup.find_all('article', class_='item')
        products_list = []

        for p in items:

            link = p.a.get('href')
            link = development_configs['modiseh']['absolute_url'] + link

            details_soup = ShopScraper._get_url_soup(lib, link)

            if not details_soup:
                return 'Error in getting url soup'

            image = details_soup.find(id='productPage').figure.img.get('src')
            persian_name = details_soup.find(id='productContent').select('.title')[0].h1.text

            from googletrans import Translator
            english_name = Translator().translate(persian_name, src='fa').text

            price = details_soup.find('meta', attrs={'itemprop':'price'}).get('content')
            brand = english_name.split(' ')[0]
            origin_shop_name = 'Modiseh'
            category = cat
            identification_in_origin = details_soup.select('.product-number')[0].text.split(':')[1]

            tech_spec = details_soup.find(class_='ac-detail').text

            entity = dict(link=link, image=image, tech_spec=tech_spec,
                            persian_name=persian_name, english_name=english_name,
                            price=price, brand=brand, category=category, id_in_origin=identification_in_origin,
                            origin_shop_name=origin_shop_name)

            products_list.append(entity)

        return products_list

    '''
        TODO: decide if convert all elements in list\
              to ScrapedProduct object, here or not
    '''
    def _get_products_on_page(self, page_soup, lib, cat):

        method_name = '_get_products_on_page__' + self.name.lower()
        try:
            method = getattr(self, method_name)
        except AttributeError:
            return 'Invalid Name'

        return method(page_soup, lib, cat)

    def convert_pretty_to_model(self, element):
        element['origin_shop_name'] = self.name
        product = ScrapedProduct(element)

        return product

    def convert_pretty_list_to_models(self, l):
        results = []
        for p in l:
            results.append(self.convert_pretty_to_model(p))

        return results
