from .scraper import ShopScraper
from .config import nicecrap_config

digikala = ShopScraper(name='Digikala',
                        url=nicecrap_config['digikala']['products_url'],
                        pagination_postfix='?pageno=')

meghdad = ShopScraper(name='Meghdad',
                        url=nicecrap_config['meghdadit']['products_url'],
                        pagination_postfix='page.')

bamilo = ShopScraper(name='Bamilo',
                        url=nicecrap_config['bamilo']['products_url'],
                        pagination_postfix='?page=')

modiseh = ShopScraper(name='Modiseh',
                        url=nicecrap_config['modiseh']['products_url'],
                        pagination_postfix='?page=')
