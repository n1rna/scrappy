'''
    ScrapedProduct Model
    ##
'''

class ScrapedProduct():

    def __init__(self, product_data):
        self.persian_name = product_data.get('persian_name', '')
        self.english_name = product_data.get('english_name', '')
        self.original_url = product_data.get('link', '')
        self.image_url = product_data.get('image', '')
        self.price = product_data.get('price', '0')
        self.brand = product_data.get('brand', '')
        self.origin_shop_name = product_data.get('origin_shop_name', '')
        self.category = product_data.get('category', '')
        self.identification_in_origin = product_data.get('id_in_origin', '')

        self.tech_spec_raw = product_data.get('tech_spec', '')

        if not self.english_name:
            translator = Translator()
            self.english_name = translator.translate(self.persian_name, src='fa').text

    def __str__(self):
        return self.english_name + ' | ' + self.persian_name
