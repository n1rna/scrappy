
nicecrap_config = {
            'digikala': {
                    'products_url' : 'https://www.digikala.com/Search/Category-mobile-phone/',
                    'absolute_url' : 'https://www.digikala.com/',
                },

            'meghdadit': {
                    'products_url' : 'https://meghdadit.com/ProductList/37/',
                    'absolute_url' : 'https://www.meghdadit.com/',
                },

            'bamilo': {
                    'products_url' : 'http://www.bamilo.com/kitchen_appliance/',
                    'absolute_url' : 'http://www.bamilo.com/',
                },

            'modiseh': {
                    'products_url' : 'https://www.modiseh.com/men/apparel/shirts',
                    'absolute_url' : 'https://www.modiseh.com/',
                },

        }

development_configs = {
            'digikala': {
                    'Cellphone' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'https://www.digikala.com/Search/Category-mobile-phone/',
                    },
                    'Home-Appliance' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'https://www.digikala.com/Search/Category-Refrigerator-Freezer',
                    },
                    'absolute_url' : 'https://www.digikala.com/',
                    'http_library': 'dryscrape',
                },

            'meghdadit': {
                    'Cellphone' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'https://meghdadit.com/ProductList/37/',
                    },
                    'absolute_url' : 'https://meghdadit.com/',
                    'http_library': 'requests',
                },

            'bamilo': {
                    'Home-Appliance' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'http://www.bamilo.com/refrigerator-freezer/',
                    },
                    'Wearable' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'http://www.bamilo.com/men_shirts/',
                    },
                    'absolute_url' : 'https://www.bamilo.com/',
                    'http_library': 'requests',
                },

            'modiseh': {
                    'Wearable' : {
                        'pages_to_scrap': 10,
                        'products_url' : 'https://www.modiseh.com/men/apparel/shirts',
                    },
                    'absolute_url' : 'https://www.modiseh.com/',
                    'http_library': 'requests',
                },
}
